class PromisableEvents {
  #events = {}
  #newEventAction = null

  constructor({ newEventAction = 'new-event-action' } = {}) {
    this.#newEventAction = newEventAction
  }

  get events() {
    return Object.keys(this.#events)
  }

  hasEvent(name) {
    return !!this.#events[name]
  }

  on(name, handler) {
    if (!this.#events[name]) {
      this.#events[name] = handler
    } else {
      throw new Error('This event already exists')
    }

    if (this.hasEvent(this.#newEventAction)) {
      this.emit(this.#newEventAction, { name, handler })
    }
  }

  off(name) {
    const hasEvent = this.hasEvent(name)
    delete this.#events[name]
    return hasEvent
  }

  emit(name, ...props) {
    return new Promise(async (resolve, reject) => {
      const handler = this.#events[name]

      if (!handler) {
        return resolve()
      }

      try {
        const result = handler(...props)
        const isPromise = !!(result && result.then && typeof result.then === 'function')

        if (isPromise) {
          try {
            resolve(await result)
          } catch (err) {
            reject(result)
          }
        } else {
          resolve(result)
        }
      } catch (err) {
        reject(err)
      }
    })
  }
}

module.exports = PromisableEvents
