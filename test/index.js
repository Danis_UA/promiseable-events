const assert = require('chai').assert
const PromisableEvents = require('../src')
const sleep = (t) => new Promise((resolve) => t ? setTimeout(resolve, t) : setImmediate(resolve)) // prettier-ignore

describe('Main test', function () {
  const pe = new PromisableEvents()
  const testData = {}

  it('Object created', function () {
    assert(pe instanceof PromisableEvents, 'Ahahah, this error is not possible')
  })

  it('Simple event should be created', function () {
    pe.on('simple-event', function (data) {
      testData.simpleEveneResult = data
    })

    assert(pe.events.includes('simple-event'), 'No event in array')
    assert(pe.hasEvent('simple-event'), 'Event not registered')
  })

  it('Event should work', function () {
    const data = { foo: 'bar' }
    pe.emit('simple-event', data)
    assert(testData.simpleEveneResult === data, "Event didn't set 'data' correctly")
  })

  it('Event works multiple times', function () {
    const anotherData = { foo: 'bar' }
    pe.emit('simple-event', anotherData)
    assert(testData.simpleEveneResult === anotherData, "Event didn't set 'anotherData' correctly")
  })

  it('Event is promisable', async function () {
    pe.on('get-promise', async function (data) {
      await sleep(40)
      return data
    })

    const result = await pe.emit('get-promise', '123QWEasd')

    assert(result === '123QWEasd', "Event didn't return expected value")
  })

  it('Should throw error if handler trows error', async function () {
    let cought = false

    pe.on('event-throws-error', async () => {
      await sleep(20)
      throw new Error('Should catch it')
    })

    await pe.emit('event-throws-error').catch((err) => {
      cought = true
    })

    assert(cought === true, "Event didn't throw error")
  })
})
